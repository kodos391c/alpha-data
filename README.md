# Alpha Data
Game data for the Alpha test of Prosperous Universe.

# Contribution Guide
Feel free to contribute to this project.

## Committing data
For JSON data, please commit after each entry that you have.

Entries that you make in a set should be committed to a new branch which may then be merged back in to the master branch once you have added all the entries that you have planned.

It is recommended to branch from the latest master prior to adding new entries.