# JSON Data
This folder may be used to store JSON format data.

# Format
Key names should be entered in lowercase, using a single underscore to separate words. 

Key names which represent tickers should be entered in upper case.

# Systems.json
This file stores system information.

# Facilities folder
This folder stores facility data sorted into files based on their category.

# Planets folder
This folder stores planet data sorted into files based on the system in which they reside.

# Recipes folder
This folder stores recipe data sorted into files based on the facility in which they may be used.

# Resources folder
This folder stores resources data sorted into files based on the category into which the resource has been assigned.